package com.zoewave.myapplication.nav

import android.util.Log
import androidx.compose.Composable
import androidx.ui.graphics.Color
import androidx.ui.material.FloatingActionButton
import androidx.ui.res.imageResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.zoewave.myapplication.R


@Composable
fun GreetingBar(name: String) {
    val onClick: () -> Unit = { Log.e("ButtonDemo", "onClick") }
    val icon = imageResource(R.drawable.plus)
    FloatingActionButton(icon = icon, onClick = onClick, color = Color.Red, elevation = 8.dp)
}

@Preview
@Composable
fun DefaultGreetingBar() {
    GreetingBar("hi")
}