package com.zoewave.myapplication.nav

sealed class Screen {
    object Home : Screen()
    data class DetailScreen(val postId: String) : Screen()
}