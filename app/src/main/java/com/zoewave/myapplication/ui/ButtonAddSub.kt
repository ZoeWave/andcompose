package com.zoewave.myapplication.ui

import androidx.compose.Composable
import androidx.ui.layout.Arrangement
import androidx.ui.layout.Row
import androidx.ui.material.Button
import androidx.ui.material.OutlinedButtonStyle
import com.zoewave.myapplication.R
import com.zoewave.myapplication.data.Info
import com.zoewave.myapplication.data.Infos

@Composable
fun ButtonAdd(infos: Infos) {
    Row(arrangement = Arrangement.SpaceEvenly) {
        Button(
            text = "Add (+) count is ${infos.inf.size}",
            onClick = {
                infos.inf.add(
                    Info(
                        R.drawable.testimg,
                        "Droid",
                        listOf("${Math.random()}", "round", "droid", "oil", "batteris")
                    )
                )
            },
            style = OutlinedButtonStyle()
        )
    }
}

@Composable
fun ButtonSub(infos: Infos) {
    Row(arrangement = Arrangement.SpaceEvenly) {
        Button(
            text = "Sub (-) count is ${infos.inf.size}",
            onClick = {
                infos.inf.remove(infos.inf[0])
            },
            style = OutlinedButtonStyle()
        )
    }
}