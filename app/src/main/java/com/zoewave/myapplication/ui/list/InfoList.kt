package com.zoewave.myapplication.ui.list

import androidx.compose.Composable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.Column
import androidx.ui.layout.Padding
import androidx.ui.unit.dp
import com.zoewave.myapplication.data.Infos

@Composable
fun InfoList(infos: Infos, myClick: (id: String) -> Unit) {
    Column {
        //Text("I've been clicked ${state.count} times")

        VerticalScroller {
            Column {
                infos.inf.forEach {
                    Padding(16.dp) {
                        InfoCard(it) { myClick(it.title) }
                    }
                }
            }
        }
    }
}