package com.zoewave.myapplication.ui.list

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.foundation.DrawImage
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.Button
import androidx.ui.material.ContainedButtonStyle
import androidx.ui.material.MaterialTheme
import androidx.ui.material.OutlinedButtonStyle
import androidx.ui.material.surface.Card
import androidx.ui.material.surface.Surface
import androidx.ui.res.imageResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.zoewave.myapplication.R
import com.zoewave.myapplication.data.Info
import com.zoewave.myapplication.data.defaultInfos

@Composable
fun InfoCard(info: Info, showAlert: (() -> Unit)) {
    //val image = +imageResource(info.imageResource) <-- (+) is removed.
    val typography = /*+*/MaterialTheme.typography()
    MaterialTheme {
        Card(shape = RoundedCornerShape(8.dp), elevation = 8.dp) {
            //Surface(shape = RoundedCornerShape(8.dp), elevation = 8.dp) {
            val image = imageResource(info.imageResource)
            Column(modifier = LayoutPadding(16.dp)) {
                Container(expanded = true, height = 144.dp) {
                    DrawImage(image = image)
                }
                Text(info.title, style = typography.h5)
                Spacer(modifier = LayoutHeight(10.dp))
                Text("Info", style = typography.subtitle2)
                for (dat in info.dats) {
                    Text(dat, style = typography.body1)
                    Spacer(modifier = LayoutHeight(2.dp))
                }
                infoButton(showAlert, info)
            }
        }
    }
}

@Composable
private fun infoButton(showAlert: () -> Unit, info: Info) {
    val typography = /*+*/MaterialTheme.typography()
    MaterialTheme {
        Spacer(modifier = LayoutHeight(20.dp))
        Surface(color = Color.LightGray) {
            Row(arrangement = Arrangement.SpaceBetween) {
                Button(
                    text = "More Info",
                    onClick = { /*do something if you want */ },
                    style = ContainedButtonStyle()
                )

                Spacer(modifier = LayoutWidth(20.dp))

                Button(
                    text = "Show Alert",
                    onClick = showAlert,
                    style = OutlinedButtonStyle()
                )

                if (info.title == "Droid") {
                    Spacer(modifier = LayoutWidth(70.dp))
                    Container(expanded = true, height = 34.dp, width = 34.dp) {
                        DrawImage(image = imageResource(R.drawable.plus))
                    }
                } else {
                    Text("NOT Droid", style = typography.h5)
                }


            }
        }
    }
}

@Composable
@Preview
fun DefaultInfoCard() {
    MaterialTheme {
        InfoCard(defaultInfos[0]) {}
    }
}