package com.zoewave.myapplication.ui

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.graphics.Color
import androidx.ui.text.TextStyle

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!", style = TextStyle(color = Color.Red))
}