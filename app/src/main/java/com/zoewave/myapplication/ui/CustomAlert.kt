package com.zoewave.myapplication.ui

import androidx.compose.Composable
import androidx.compose.state
import androidx.ui.core.Text
import androidx.ui.material.AlertDialog
import androidx.ui.material.AlertDialogButtonLayout
import androidx.ui.material.Button

@Composable
fun ShowAlertDialog(title: String) {
    val openDialog = state { true }
    if (openDialog.value) {
        AlertDialog(
            onCloseRequest = {
            },
            title = {
                Text(text = title)
            },
            text = {
                Text("This is alert dialog in jetpack compose!")
            },
            confirmButton = {
                Button("Confirm", onClick = {
                    openDialog.value = false
                })
            },
            dismissButton = {
                Button("Dismiss", onClick = {
                    openDialog.value = false
                })
            },
            buttonLayout = AlertDialogButtonLayout.Stacked
        )
    }

}