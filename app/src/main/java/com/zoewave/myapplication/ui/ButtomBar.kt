package com.zoewave.myapplication.ui

import androidx.compose.Composable
import androidx.ui.graphics.Image
import androidx.ui.material.AppBarIcon
import androidx.ui.material.BottomAppBar
import androidx.ui.res.imageResource
import com.zoewave.myapplication.R

@Composable
fun BottomAppBarNoFab() {
    val someActionImage: Image = imageResource(
        R.drawable.plus
    )
    val someNavigationImage: Image = imageResource(R.drawable.plus)
    val navigationIcon: @Composable() () -> Unit = {
        AppBarIcon(someNavigationImage) { /* doSomething()*/ }
    }
    val actionData = listOf(someActionImage)
    BottomAppBar(
        navigationIcon = navigationIcon,
        actionData = actionData
    ) { actionImage ->
        AppBarIcon(actionImage) { /* doSomething()*/ }
    }
}