package com.zoewave.myapplication.ui

import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.material.Checkbox

@Model
class FormState(var optionChecked: Boolean)

@Composable
fun Form(formState: FormState) {
    Checkbox(
        checked = formState.optionChecked,
        onCheckedChange = { newState -> formState.optionChecked = newState })
}