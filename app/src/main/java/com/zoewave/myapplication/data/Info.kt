package com.zoewave.myapplication.data

import androidx.annotation.DrawableRes

data class Info(
    @DrawableRes val imageResource: Int,
    val title: String,
    val dats: List<String>
)