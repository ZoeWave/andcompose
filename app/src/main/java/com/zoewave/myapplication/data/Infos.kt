package com.zoewave.myapplication.data

import androidx.compose.Model
import androidx.compose.frames.ModelList
import com.zoewave.myapplication.R


/**
https://kotlinlang.slack.com/archives/CJLTWPH7S/p1580393444135600?thread_ts=1580393049.135300&cid=CJLTWPH7S

@Model observation is shallow, it will only make changes to the class annotated with it
observable. If you change a plain old mutable object that happens to be held in a val
in an @Model class, that change won't be tracked.

If you want to detect changes in the content of a mutable list, yes, that's what
ModelList is for. Alternatively if you're using immutable lists and mutating a
reference to said immutable list in a @Model class, that will also do what you're after
 */
@Model
data class Infos(var inf: ModelList<Info> = ModelList()) {
    // initializer block
    init {
        defaultInfos.forEach {
            inf.add(it)
        }
    }
}

val defaultInfos = listOf(
    Info(
        R.drawable.testimg,
        "Droid",
        listOf("green", "round", "droid", "oil", "batteris")
    ),
    Info(
        R.drawable.testimg,
        "House",
        listOf("door", "window", "living")
    ),
    Info(
        R.drawable.testimg,
        "Bank",
        listOf("money", "corrupt", "greedy", "rich", "love")
    ),
    Info(
        R.drawable.testimg,
        "library",
        listOf("Hate")
    ),
    Info(
        R.drawable.testimg,
        "Icon",
        listOf("black", "round", "simpleton")
    )
)