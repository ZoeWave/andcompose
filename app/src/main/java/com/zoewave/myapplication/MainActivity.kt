package com.zoewave.myapplication

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.state
import androidx.ui.core.Text
import androidx.ui.core.setContent
import androidx.ui.graphics.Color
import androidx.ui.layout.Arrangement
import androidx.ui.layout.Column
import androidx.ui.layout.Padding
import androidx.ui.layout.Row
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.zoewave.myapplication.data.Infos
import com.zoewave.myapplication.ui.ButtonAdd
import com.zoewave.myapplication.ui.ButtonSub
import com.zoewave.myapplication.ui.list.InfoList

// list data to the top level
val currInfoList = Infos()

@Composable
// state is same as @Model
val amount = state { 0 } // +observe(amount)

// Material Theme Composable
// get your themeStyels when you need them

class MainActivity : AppCompatActivity() {

    //One important point is that data is always passed down from the parent and events are always passed up from the widgets.
    //declarative UI. Properties down, events up so that you have a single source of truth.
    var myClick = { id: String ->
        Toast.makeText(applicationContext, "You just the $id", Toast.LENGTH_LONG)
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            mainView(currInfoList, myClick)
        }
    }
}

@Composable
fun mainView(infoState: Infos, myClick: (id: String) -> Unit) {
    MaterialTheme {
        Column {

            Padding(padding = 12.dp) {
                Row(arrangement = Arrangement.SpaceBetween) {
                    ButtonAdd(infos = currInfoList)
                    Padding(padding = 12.dp) {
                        ButtonSub(infos = currInfoList)
                    }
                    Surface(color = Color.LightGray) {
                        Text("count =  ${currInfoList.inf.size}")
                    }
                }

            }
            //GreetingBar("hi")
            /*TopAppBar(title = {
            Text("ComposableInfo")
        })*/
            Column {
                InfoList(infoState, myClick)
            }
        }
    }
}


@Preview
@Composable
fun DefaultPreview() {
    mainView(infoState = currInfoList, myClick = {})
}